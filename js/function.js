// Создайте здесь свои функции
let pickWord = function () {
    word = words[Math.floor(Math.random() * words.length)];// Возвращает случайно выбранное слово
};
let setupAnswerArray = function (word) {
    return answerArray = [];
};
let showPlayerProgress = function (answerArray) {
    for (let i = 0; i < word.length; i++) {
        answerArray[i] = "_";
    }// С помощью alert отображает текущее состояние игры
};
let getGuess = function () {
    // Запрашивает ответ игрока с помощью prompt
};
let updateGameState = function (guess, word, answerArray) {
    // Обновляет answerArray согласно ответу игрока (guess)
    // возвращает число, обозначающее, сколько раз буква guess
    // встречается в слове, чтобы можно было обновить значение
    // remainingLetters
};
let showAnswerAndCongratulatePlayer = function (answerArray) {
    // С помощью alert показывает игроку отгаданное слово
    // и поздравляет его с победой
};

// word: загаданное слово
let word = pickWord();
// answerArray: итоговый массив
let answerArray = setupAnswerArray(word);
// remainingLetters: сколько букв осталось угадать
let remainingLetters = word.length;
while (remainingLetters > 0) {
    showPlayerProgress(answerArray);
    // guess: ответ игрока
    let guess = getGuess();
    if (guess === null) {
        break;
    } else if (guess.length !== 1) {
        alert("Пожалуйста, введите одиночную букву.");
    } else {
        // correctGuesses: количество открытых букв
        let correctGuesses = updateGameState(guess, word,
            answerArray);
        remainingLetters -= correctGuesses;
    }
}
showAnswerAndCongratulatePlayer(answerArray);